//
//  HomeCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockButton) ();
@interface HomeCell : UITableViewCell

+(instancetype)homeCellWithTableView:(UITableView *)tableView;
@property (weak, nonatomic) IBOutlet UIImageView *homeIconImage;
@property (weak, nonatomic) IBOutlet UILabel *homeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeSexLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeAgeLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeMoneyLabel;
@property (weak, nonatomic) IBOutlet UILabel *ChatDeclarationLable;
@property (weak, nonatomic) IBOutlet UIButton *homePhoneBtn;
- (IBAction)homePhoneBtnAct:(id)sender;

@property (strong,nonatomic) BlockButton button;

-(void)clickHomePhoneBtnAct:(BlockButton)block;

@end
