//
//  HomeCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "HomeCell.h"

@implementation HomeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //    self.homeIconImage.layer.cornerRadius = 35;
    //    self.homeIconImage.layer.borderWidth = 2;
    //    self.homeIconImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)homeCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"HomeCell";
    HomeCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:self options:nil] lastObject];
    }
    cell.homeNameLabel.text = @"isfihjcjshfuifheuin";
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (IBAction)homePhoneBtnAct:(id)sender {
    if (self.button) {
        self.button();
    }
}

-(void)clickHomePhoneBtnAct:(BlockButton)block
{
    self.button = block;
}

@end
