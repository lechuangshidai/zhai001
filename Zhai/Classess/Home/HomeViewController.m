//
//  HomeViewController.m
//  Zhai
//
//  Created by 周文静 on 16/7/28.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "HomeViewController.h"
#import "EnterViewController.h"
#import "HomeCell.h"
#import "PhoneSelfRoomViewController.h"
#import "AFManager.h"
#import "UIImageView+AFNetworking.h"
@interface HomeViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArr;
}
@property (weak, nonatomic) IBOutlet UITableView *homeTable;
@end

@implementation HomeViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 删除系统自带的tabBarButton
    for (UIView *tabBarButton in self.tabBarController.tabBar.subviews) {
        
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            
            [tabBarButton removeFromSuperview];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"妹妹陪你聊聊天";
    dataArr = [[NSMutableArray alloc]init];
    [self setExtraCellLineHidden:self.homeTable];
    [AFManager getReqURL:[NSString stringWithFormat:@"%@?uid=%@&page=%@&num=%@",SHOUYE,@"11",@"0",@"15"] block:^(id infor) {
        NSLog(@"--%@",infor);
        [dataArr addObjectsFromArray:infor[@"data"]];
        [self.homeTable reloadData];
    } errorblock:^(NSError *error) {
        NSLog(@"--%@--",error);
    }];
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HomeCell *cell = [HomeCell homeCellWithTableView:tableView];
    cell.homeNameLabel.text = dataArr[indexPath.row][@"nickname"];
    cell.homeAgeLabel.text =[NSString stringWithFormat:@"%@岁",dataArr[indexPath.row][@"birthday"]];
    if ([[NSString stringWithFormat:@"%@",dataArr[indexPath.row][@"auto"]] length]>0) {
        cell.ChatDeclarationLable.text = dataArr[indexPath.row][@"auto"];

    }else{
        cell.ChatDeclarationLable.text = @"太懒了，还没有宣言。";


    }
    if ([[NSString stringWithFormat:@"%@",dataArr[indexPath.row][@"sex"]] isEqualToString:@"0"]) {
        cell.homeSexLabel.text = @"男";
    }else if ([[NSString stringWithFormat:@"%@",dataArr[indexPath.row][@"sex"]] isEqualToString:@"1"]){
        cell.homeSexLabel.text = @"女";
    }else{
  
        cell.homeSexLabel.text = @"未知";
    }
    
    [cell.homeIconImage setImageWithURL:[NSURL URLWithString:dataArr[indexPath.row][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
    cell.homeIconImage.layer.masksToBounds = YES;
    cell.homeIconImage.layer.cornerRadius = 35;

    [cell clickHomePhoneBtnAct:^{
        
        NSLog(@"点击了 %ld 打电话",(long)indexPath.row);
    }];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    PhoneSelfRoomViewController *phoneSelfRoomVC = [[PhoneSelfRoomViewController alloc]initWithNibName:@"PhoneSelfRoomViewController" bundle:nil];
    phoneSelfRoomVC.uid = dataArr[indexPath.row][@"id"];
    [self.navigationController pushViewController:phoneSelfRoomVC animated:YES];
    


}

//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}


@end
