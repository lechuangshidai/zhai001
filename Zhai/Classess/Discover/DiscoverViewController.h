//
//  DiscoverViewController.h
//  Zhai
//
//  Created by 周文静 on 16/7/28.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscoverViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *bgImage;

@property (weak, nonatomic) IBOutlet UIButton *paoBtn;
- (IBAction)paoBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *oneIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *twoIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *threeIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *fourIconImage;
@property (weak, nonatomic) IBOutlet UIImageView *fiveIconImage;

@end
