//
//  DiscoverViewController.m
//  Zhai
//
//  Created by 周文静 on 16/7/28.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "DiscoverViewController.h"
#import "AFManager.h"
#import "UIImageView+WebCache.h"

#define bigIconAnima 1.3
#define smallIconAnima  0.1

@interface DiscoverViewController ()
{
    NSMutableArray *dataArr;
}
@end

@implementation DiscoverViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 删除系统自带的tabBarButton
    for (UIView *tabBarButton in self.tabBarController.tabBar.subviews) {
        
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            
            [tabBarButton removeFromSuperview];
        }
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    dataArr = [[NSMutableArray alloc]init];
    self.navigationController.navigationBar.hidden = YES;
    self.title = @"发现";
    
    self.bgImage.frame = CGRectMake(0, 0, WJScreenW, WJScreenH);
    
    CGFloat imageW = WJScreenW * 0.35;
    CGFloat imageH = imageW;
    
    CGFloat oneIconImageX = WJScreenW * 0.38;
    CGFloat twoIconImageX = WJScreenW * 0.07;
    CGFloat threeIconImageX = WJScreenW * 0.6;
    CGFloat fourIconImageX = WJScreenW * 0.1;
    CGFloat fiveIconImageX = WJScreenW * 0.5;
    
    CGFloat oneIconImageY = WJScreenH * 0.12;
    CGFloat twoIconImageY = WJScreenH * 0.25;
    CGFloat threeIconImageY = WJScreenH * 0.30;
    CGFloat fourIconImageY = WJScreenH * 0.47;
    CGFloat fiveIconImageY = WJScreenH * 0.52;
    
    [AFManager getReqURL:[NSString stringWithFormat:@"%@?uid=%@&page=%@&num=%@",SHOUYE,@"11",@"0",@"15"] block:^(id infor) {
        NSLog(@"--%@",infor);
        for (int i =0; i<5; i++) {
           int j= arc4random()%[infor[@"data"] count];
            
            [dataArr addObject:infor[@"data"][j]];
        }
        [self.oneIconImage setImageWithURL:[NSURL URLWithString:dataArr[0][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.twoIconImage setImageWithURL:[NSURL URLWithString:dataArr[1][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.threeIconImage setImageWithURL:[NSURL URLWithString:dataArr[2][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.fourIconImage setImageWithURL:[NSURL URLWithString:dataArr[3][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.fiveIconImage setImageWithURL:[NSURL URLWithString:dataArr[4][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        self.oneIconImage.layer.masksToBounds = YES;
        self.oneIconImage.layer.cornerRadius = 60;
        self.twoIconImage.layer.masksToBounds = YES;
        self.twoIconImage.layer.cornerRadius = 60;
        self.threeIconImage.layer.masksToBounds = YES;
        self.threeIconImage.layer.cornerRadius = 60;
        self.fourIconImage.layer.masksToBounds = YES;
        self.fourIconImage.layer.cornerRadius = 60;
        self.fiveIconImage.layer.masksToBounds = YES;
        self.fiveIconImage.layer.cornerRadius = 60;
        NSLog(@"%@",dataArr);
        
    } errorblock:^(NSError *error) {
        NSLog(@"--%@--",error);
    }];
    self.oneIconImage.frame = CGRectMake(oneIconImageX, oneIconImageY,imageW, imageH);
    self.twoIconImage.frame = CGRectMake(twoIconImageX, twoIconImageY, imageW, imageH);
    self.threeIconImage.frame = CGRectMake(threeIconImageX, threeIconImageY, imageW, imageH);
    self.fourIconImage.frame = CGRectMake(fourIconImageX, fourIconImageY, imageW, imageH);
    self.fiveIconImage.frame = CGRectMake(fiveIconImageX, fiveIconImageY, imageW, imageH);
    
    self.paoBtn.frame = CGRectMake((WJScreenW-142)*0.5, WJScreenH*0.84, 142, 34);
    
    
    
    
}



- (IBAction)paoBtnAct:(id)sender {
    [dataArr removeAllObjects];
    [AFManager getReqURL:[NSString stringWithFormat:@"%@?uid=%@&page=%@&num=%@",SHOUYE,@"11",@"0",@"15"] block:^(id infor) {
        NSLog(@"--%@",infor);
        for (int i =0; i<5; i++) {
            int j= arc4random()%[infor[@"data"] count];
            
            [dataArr addObject:infor[@"data"][j]];
        }
        [self.oneIconImage setImageWithURL:[NSURL URLWithString:dataArr[0][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.twoIconImage setImageWithURL:[NSURL URLWithString:dataArr[1][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.threeIconImage setImageWithURL:[NSURL URLWithString:dataArr[2][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.fourIconImage setImageWithURL:[NSURL URLWithString:dataArr[3][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        [self.fiveIconImage setImageWithURL:[NSURL URLWithString:dataArr[4][@"head_pic"]] placeholderImage:[UIImage imageNamed:@"fx-tx5"]];
        self.oneIconImage.layer.masksToBounds = YES;
        self.oneIconImage.layer.cornerRadius = 60;
        self.twoIconImage.layer.masksToBounds = YES;
        self.twoIconImage.layer.cornerRadius = 60;
        self.threeIconImage.layer.masksToBounds = YES;
        self.threeIconImage.layer.cornerRadius = 60;
        self.fourIconImage.layer.masksToBounds = YES;
        self.fourIconImage.layer.cornerRadius = 60;
        self.fiveIconImage.layer.masksToBounds = YES;
        self.fiveIconImage.layer.cornerRadius = 60;
        NSLog(@"%@",dataArr);
        
    } errorblock:^(NSError *error) {
        NSLog(@"--%@--",error);
    }];

    [self paopapBtnActAnima];
    
    [self iconImageAnima];
}


-(void)paopapBtnActAnima
{
    self.paoBtn.transform = CGAffineTransformIdentity;
    [UIView animateKeyframesWithDuration:0.2 delay:0 options:0 animations: ^{
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
            self.paoBtn.transform = CGAffineTransformMakeScale(1.5, 1.5);
        }];
        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
            self.paoBtn.transform = CGAffineTransformMakeScale(0.8, 0.8);
        }];
        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
            self.paoBtn.transform = CGAffineTransformMakeScale(1.0, 1.0);
        }];
    } completion:nil];
    
}

-(void)iconImageAnima
{
    self.oneIconImage.transform = CGAffineTransformIdentity;
    self.twoIconImage.transform = CGAffineTransformIdentity;
    self.threeIconImage.transform = CGAffineTransformIdentity;
    self.fourIconImage.transform = CGAffineTransformIdentity;
    self.fiveIconImage.transform = CGAffineTransformIdentity;
    
//    [UIView animateKeyframesWithDuration:1.0 delay:0 options:0 animations: ^{
//        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
//
//            self.oneIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.twoIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.threeIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.fourIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.fiveIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            
//        }];
//        [UIView addKeyframeWithRelativeStartTime:1/3.0 relativeDuration:1/3.0 animations: ^{
//
//            self.oneIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
//            self.twoIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
//            self.threeIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
//            self.fourIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
//            self.fiveIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
//        }];
//        [UIView addKeyframeWithRelativeStartTime:2/3.0 relativeDuration:1/3.0 animations: ^{
//
//            self.oneIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
//            self.twoIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
//            self.threeIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
//            self.fourIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
//            self.fiveIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
//
//        }];
//    } completion:nil];

    
    [UIView animateKeyframesWithDuration:1.0 delay:0 options:0 animations: ^{
//        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1 / 3.0 animations: ^{
//            
//            self.oneIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.twoIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.threeIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.fourIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            self.fiveIconImage.transform = CGAffineTransformMakeScale(bigIconAnima, bigIconAnima);
//            
//        }];
        [UIView addKeyframeWithRelativeStartTime:0 relativeDuration:1/3.0 animations: ^{
            
            self.oneIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
            self.twoIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
            self.threeIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
            self.fourIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
            self.fiveIconImage.transform = CGAffineTransformMakeScale(smallIconAnima, smallIconAnima);
        }];
        [UIView addKeyframeWithRelativeStartTime:0.5 relativeDuration:1/3.0 animations: ^{
            
            self.oneIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.twoIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.threeIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.fourIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            self.fiveIconImage.transform = CGAffineTransformMakeScale(1.0, 1.0);
            
        }];
    } completion:nil];

}

@end
