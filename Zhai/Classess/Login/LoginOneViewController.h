//
//  LoginOneViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKCountDownButton.h"

@interface LoginOneViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *yanzhengNumber;
@property (weak, nonatomic) IBOutlet UITextField *passwordNumber;
@property (weak, nonatomic) IBOutlet UITextField *againPasswordNumber;

- (IBAction)nextBtnAct:(id)sender;

@property (weak, nonatomic) IBOutlet JKCountDownButton *yanzhengmaBtn;
- (IBAction)yanzhengmaBtnAct:(JKCountDownButton*)sender;

@end
