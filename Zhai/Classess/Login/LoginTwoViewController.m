//
//  LoginTwoViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "LoginTwoViewController.h"
#import "UIImage+DY.h"
#import "ZHPickView.h"
#import "TabBarController.h"
#import "AFManager.h"

@interface LoginTwoViewController ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,ZHPickViewDelegate>
@property(nonatomic,strong)ZHPickView *pickview;
@property (strong ,nonatomic)UIImagePickerController *imagePikcerController;

@property (assign,nonatomic) BOOL isyes;
@property(nonatomic,assign)NSInteger  btnValue;
@property(nonatomic,copy)NSString *uploadImageUrl;

@end

@implementation LoginTwoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    self.title = @"完善个人信息";
    self.yearView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.yearView.layer.borderWidth = 1.0f;
    self.monthView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.monthView.layer.borderWidth = 1.0f;
    self.dayView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.dayView.layer.borderWidth = 1.0f;
    
    self.nameTextField.delegate = self;
}


- (IBAction)iconBtnAct:(id)sender {
    
    
    [NSObject wj_selVcWithTitle:@"请选择头像" TitleExplain:@"" FirstSel:@"拍照" SecondSel:@"从手机相册选择" SelfVc:self PresentStyle:WJNewPresentFromBottom FirstOrSureBlock:^(NSString *userSelStr) {
           [self openCamera];
    } SecondSelOrCancelBlock:^(NSString *userSelStr) {
         [self openAlbum];
    }];

}

//打开照相机
-(void)openCamera
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) return;
    
    UIImagePickerController *ipc = [[UIImagePickerController alloc]init];
    ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
    ipc.delegate = self;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

//打开相册
-(void)openAlbum
{
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) return;
    
    UIImagePickerController *ipc = [[UIImagePickerController alloc]init];
    ipc.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    ipc.delegate = self;
    ipc.allowsEditing = YES;
    [self presentViewController:ipc animated:YES completion:nil];
}

#pragma mark UIImagePickerControllerDelegate
-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    UIImage *image = [info objectForKey:UIImagePickerControllerEditedImage];
    [picker dismissViewControllerAnimated:YES completion:nil];
    [self uploadImage:image];
    if(self.imagePikcerController.sourceType == UIImagePickerControllerSourceTypeCamera) {
        [self saveImage:image];
    }
    CGSize size = CGSizeMake(90, 90);
    NSData *cutData = [UIImage imageWithImage:image scaledToSize:size];
    _iconImage.image = [UIImage circleWithImage:[UIImage imageWithData:cutData] borderWidth:3.5 borderColor:[UIColor whiteColor]];
}

- (void)saveImage:(UIImage *)image{
    UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil);
}




- (IBAction)dataPickerBtnAct:(id)sender {
    [_pickview remove];
    NSDate *date=[NSDate dateWithTimeIntervalSinceNow:9000000];
    _pickview=[[ZHPickView alloc] initDatePickWithDate:date datePickerMode:UIDatePickerModeDate isHaveNavControler:NO];
    self.pickview.delegate = self;
    [_pickview show];
}


- (IBAction)manBtnAct:(id)sender {
#pragma mark --self.manBtn.tag == 1001
    _btnValue = self.manBtn.tag;
    if (self.isyes==NO){
   
        [self.manBtn setImage:[UIImage imageNamed:@"hongdian"] forState:UIControlStateNormal];
        [self.womanBtn setImage:[UIImage imageNamed:@"huiquan"] forState:UIControlStateNormal];

        
    }else if (self.isyes==YES) {

        [self.manBtn setImage:[UIImage imageNamed:@"hongdian"] forState:UIControlStateNormal];
        [self.womanBtn setImage:[UIImage imageNamed:@"huiquan"] forState:UIControlStateNormal];
    }
    self.isyes = !self.isyes;
}

- (IBAction)womanBtnAct:(id)sender {
#pragma mark --self.womanBtn.tag == 1000
    _btnValue = self.womanBtn.tag;
    if (self.isyes==NO){
        
        [self.womanBtn setImage:[UIImage imageNamed:@"hongdian"] forState:UIControlStateNormal];
        [self.manBtn setImage:[UIImage imageNamed:@"huiquan"] forState:UIControlStateNormal];
        
    }else if (self.isyes==YES) {
        
        [self.womanBtn setImage:[UIImage imageNamed:@"hongdian"] forState:UIControlStateNormal];
        [self.manBtn setImage:[UIImage imageNamed:@"huiquan"] forState:UIControlStateNormal];
    }
    self.isyes = !self.isyes;
}
- (IBAction)successBtnAct:(id)sender {

    
    [self postRequestInfo];
    [[NSNotificationCenter defaultCenter]postNotificationName:@"login" object:nil];
    
}
-(void)uploadImage:(UIImage *)image
{
    NSData *data=UIImageJPEGRepresentation(image, 1);
    [AFManager upLoadpath:KURL_FORMAT(LOCAL_HOST_URL,  Upload_Image_Url) reqBody:nil file:data fileName:@"file" fileType:@"image/jpg" block:^(id infor) {
        NSLog(@"====%@",infor);
        NSString *inforStr = [infor stringByReplacingOccurrencesOfString:@"\\" withString:@""];
        NSString *inforStr2 = [inforStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        _uploadImageUrl = inforStr2;
//        "http:\/\/139.224.43.42\/zhai\/Uploads\/18b9c171ee7bd29789670f66721b5984.png"
        NSLog(@"url===%@",_uploadImageUrl);
    } errorBlock:^(NSError *error) {
        
    }];
}

-(void)postRequestInfo
{
    NSMutableDictionary *bodyDic = [[NSMutableDictionary alloc] init];
    [bodyDic setObject:_uploadImageUrl forKey:@"head_pic"];
//    [bodyDic setObject:@"11" forKey:@"id"];
     [bodyDic setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"uid"] forKey:@"id"];
    [bodyDic setObject:_nameTextField.text forKey:@"nickname"];
    [bodyDic setObject:[NSString stringWithFormat:@"%ld",_btnValue-1000] forKey:@"sex"];
    [bodyDic setObject:[NSString stringWithFormat:@"%@-%@-%@",self.yearLabel.text,self.monthLabel.text,self.dayLabel.text] forKey:@"birthday"];
    NSLog(@"bodyDic===%@",bodyDic);
    [AFManager postReqURL:KURL_FORMAT(LOCAL_HOST_URL, Personal_Information_URL) reqBody:bodyDic block:^(id infor) {
        NSLog(@"infor===%@",infor);
        if ([[NSString stringWithFormat:@"%@",infor[@"code"]] isEqualToString:@"200"]) {
            
            [[NSNotificationCenter defaultCenter]postNotificationName:@"login" object:nil];
        }
    }];
}
#pragma mark ZhpickVIewDelegate--选择年月日
-(void)toobarDonBtnHaveClick:(ZHPickView *)pickView resultString:(NSString *)resultString{
    
        NSString *strYear = [resultString substringToIndex:4];
    
        NSRange rangeMonth = {5,2};
        NSString *strMonth = [resultString substringWithRange:rangeMonth];
    
        NSRange rangeDay = {8,2};
        NSString *strDay = [resultString substringWithRange:rangeDay];
    
        self.yearLabel.text = [NSString stringWithFormat:@"%@年",strYear];
        self.monthLabel.text = [NSString stringWithFormat:@"%@月",strMonth];
        self.dayLabel.text = [NSString stringWithFormat:@"%@日",strDay];
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
