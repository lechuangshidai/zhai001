//
//  EnterViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EnterViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *enterBtn;
- (IBAction)enterBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *loginBtn;
- (IBAction)loginBtnAct:(id)sender;
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *passwordNumber;
@end
