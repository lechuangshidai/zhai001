//
//  EnterViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "EnterViewController.h"
#import "LoginOneViewController.h"
#import "LoginTwoViewController.h"

#import "TabBarController.h"
#import "HomeViewController.h"
#import "DiscoverViewController.h"
#import "NearbyViewController.h"
#import "MyViewController.h"
#import "NavViewController.h"
#import "AFManager.h"
@interface EnterViewController ()<UITextFieldDelegate>

@end

@implementation EnterViewController
//-(void)viewWillAppear:(BOOL)animated
//{
//    [super viewWillAppear:animated];
//    TabBarController *tabbar = [[TabBarController alloc] init];
//    [tabbar hideTabbar];
//}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"登录";
    [self.navigationItem setHidesBackButton:YES];
    UIBarButtonItem *item = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@""] style:UIBarButtonItemStyleDone target:self action:nil];
    self.navigationItem.leftBarButtonItem = item;
   //设置textfiled的placeholder
    [self setTextFiledPlaceholder];


    
}

-(void)setTextFiledPlaceholder
{
    self.phoneNumber.placeholder = @"手机号";
    [self.phoneNumber setValue:[UIColor wjColorFloat:@"f700ff"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.phoneNumber setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    self.phoneNumber.delegate = self;

    
    self.passwordNumber.placeholder = @"密码";
    [self.passwordNumber setValue:[UIColor wjColorFloat:@"f700ff"] forKeyPath:@"_placeholderLabel.textColor"];
    [self.passwordNumber setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    self.passwordNumber.delegate = self;

}


- (IBAction)enterBtnAct:(id)sender {
    
    NSString *strphoneNumber = self.phoneNumber.text;
    if ([strphoneNumber isValidPhoneNumber]) {
        
        NSLog(@"手机号格式正确");

    }else{
        [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"请输入正确的手机号" andSelfVC:self];
    }

    [self postRegister];
}
-(void)postRegister
{
    NSMutableDictionary *bodyDic = [[NSMutableDictionary alloc] init];
    [bodyDic setObject:self.phoneNumber.text forKey:@"phone"];
//    [bodyDic setObject:self.yanzhengNumber.text forKey:@"code"];
    [bodyDic setObject:self.passwordNumber.text forKey:@"password"];
//    [bodyDic setObject:self.againPasswordNumber.text forKey:@"repass"];
    NSLog(@"%@",KURL_FORMAT(LOCAL_HOST_URL,LOGIN_URL));
    [AFManager postReqURL:KURL_FORMAT(LOCAL_HOST_URL,LOGIN_URL) reqBody:bodyDic block:^(id infor) {
        NSLog(@"infor====%@",infor);
        if ([[NSString stringWithFormat:@"%@",infor[@"code"]] isEqualToString:@"200"]) {
            [[NSUserDefaults standardUserDefaults]setValue:infor[@"uid"] forKey:@"uid"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"login" object:nil];
        }
    }];
}
- (IBAction)loginBtnAct:(id)sender {
    
    LoginOneViewController *loginOneVC = [[LoginOneViewController alloc]initWithNibName:@"LoginOneViewController" bundle:nil];
    
    [self.navigationController pushViewController:loginOneVC animated:YES];
    
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}


@end
