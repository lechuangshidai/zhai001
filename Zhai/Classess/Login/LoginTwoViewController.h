//
//  LoginTwoViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginTwoViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
- (IBAction)iconBtnAct:(id)sender;

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;

@property (weak, nonatomic) IBOutlet UIView *yearView;
@property (weak, nonatomic) IBOutlet UIView *monthView;
@property (weak, nonatomic) IBOutlet UIView *dayView;

- (IBAction)dataPickerBtnAct:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;



@property (weak, nonatomic) IBOutlet UIButton *manBtn;
@property (weak, nonatomic) IBOutlet UIButton *womanBtn;
- (IBAction)manBtnAct:(id)sender;
- (IBAction)womanBtnAct:(id)sender;

- (IBAction)successBtnAct:(id)sender;
@end
