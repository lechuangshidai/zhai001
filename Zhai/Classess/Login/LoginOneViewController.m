//
//  LoginOneViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/4.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "LoginOneViewController.h"
#import "LoginTwoViewController.h"
#import <SMS_SDK/SMSSDK.h>
#import "AFManager.h"
@interface LoginOneViewController ()<UITextFieldDelegate>

@end

@implementation LoginOneViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"注册";
    
    //设置textField的边框颜色
    [self setTextFieldBorder];
}

-(void)setTextFieldBorder
{
    self.phoneNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.phoneNumber.layer.borderWidth = 1.0;
    [self.phoneNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.yanzhengNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.yanzhengNumber.layer.borderWidth = 1.0;
    
    self.passwordNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.passwordNumber.layer.borderWidth = 1.0;
    self.passwordNumber.delegate = self;
    
    self.againPasswordNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.againPasswordNumber.layer.borderWidth = 1.0;
    self.againPasswordNumber.delegate = self;
}

-(void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneNumber) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringFromIndex:11];
        }
    }
}

- (IBAction)nextBtnAct:(id)sender {
    //18236752947   13552529660
    [self postRegister];


    [SMSSDK commitVerificationCode:self.yanzhengNumber.text phoneNumber:self.phoneNumber.text zone:@"86" result:^(SMSSDKUserInfo *userInfo, NSError *error) {
            if (!error)
            {
                NSLog(@"验证成功");
                                if (![self.passwordNumber.text isEqualToString:@""]&&[self.passwordNumber.text isEqualToString:self.againPasswordNumber.text])
                {
//                    [self postRegister];
                    

                }
                else if([self.passwordNumber.text isEqualToString:@""])
                {
                    [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"密码不能为空" andSelfVC:self];
                }
                else if (![self.passwordNumber.text isEqualToString:self.againPasswordNumber.text])
                {
                     [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"两次密码不一致" andSelfVC:self];
                }
                else
                {
                    
                }
            }
            else
            {
                [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"验证码不正确" andSelfVC:self];
                NSLog(@"错误信息:%@",error);
            }
    }];
   
}

- (IBAction)yanzhengmaBtnAct:(JKCountDownButton*)sender {
  
    
    NSString *strphoneNumber = self.phoneNumber.text;

    
    [SMSSDK getVerificationCodeByMethod:SMSGetCodeMethodSMS phoneNumber:strphoneNumber zone:@"86" customIdentifier:nil result:^(NSError *error){
        if (!error)
        {
            NSLog(@"获取验证码成功");
        }
        else
        {
            NSLog(@"错误信息：%@",error);
        }
    }];
    if ([strphoneNumber isValidPhoneNumber]) {
        
        sender.enabled = NO;
        //button type要 设置成custom 否则会闪动
        [sender startWithSecond:60];
        
        [sender didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
            NSString *title = [NSString stringWithFormat:@"剩余%d秒",second];
            return title;
        }];
        [sender didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            countDownButton.enabled = YES;
            return @"点击重新获取";
            
        }];

        
    }else{
        [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"请输入正确的手机号" andSelfVC:self];
    }
    
    
    
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}


#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}
-(void)postRegister
{
    NSMutableDictionary *bodyDic = [[NSMutableDictionary alloc] init];
    [bodyDic setObject:self.phoneNumber.text forKey:@"phone"];
    [bodyDic setObject:self.yanzhengNumber.text forKey:@"code"];
    [bodyDic setObject:self.passwordNumber.text forKey:@"password"];
    [bodyDic setObject:self.againPasswordNumber.text forKey:@"repass"];
    [AFManager postReqURL:KURL_FORMAT(LOCAL_HOST_URL,REGISTER_URL) reqBody:bodyDic block:^(id infor) {
        NSLog(@"infor====%@",infor);
        if ([[NSString stringWithFormat:@"%@",infor[@"code"]] isEqualToString:@"200"]) {
    [[NSUserDefaults standardUserDefaults]setValue:infor[@"id"] forKey:@"uid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
            LoginTwoViewController *loginTwoVC = [[LoginTwoViewController alloc]initWithNibName:@"LoginTwoViewController" bundle:nil];
            [self.navigationController pushViewController:loginTwoVC animated:YES];
        }
    }];
}

@end
