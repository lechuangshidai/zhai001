//
//  TixianCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/15.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TixianCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsLabel;

@property (weak, nonatomic) IBOutlet UIImageView *selecteImage;

+(instancetype)tixianViewCellWithTableView:(UITableView *)tableView;

@end
