//
//  TixianCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/15.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "TixianCell.h"

@implementation TixianCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)tixianViewCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"TixianCell";
    TixianCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TixianCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
