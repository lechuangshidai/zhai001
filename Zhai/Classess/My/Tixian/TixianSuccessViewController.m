//
//  TixianSuccessViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/15.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "TixianSuccessViewController.h"
#import "MyViewController.h"

@interface TixianSuccessViewController ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
- (IBAction)finishBtnAct:(id)sender;

@end

@implementation TixianSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"提现详情";
}



- (IBAction)finishBtnAct:(id)sender {
    
    [self.navigationController popToRootViewControllerAnimated:YES];

}
@end
