//
//  TixianViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/15.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "TixianViewController.h"
#import "TixianCell.h"
#import "TixianSuccessViewController.h"


@interface TixianViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tixainTable;
@property (strong,nonatomic) UIView *headerView;
@property (strong,nonatomic) UIView *footerView;

@property (strong,nonatomic) NSMutableArray *cellArr;

@end

@implementation TixianViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"提现";
    
    [self getHeaderView];
    
    [self getFooterView];
    
    _tixainTable.tableHeaderView = self.headerView;
    _tixainTable.tableFooterView = self.footerView;
    
    self.cellArr = [[NSMutableArray alloc]initWithCapacity:10];
    
}

-(void)getHeaderView
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 75)];
    self.headerView = headerView;
    
    UIView *grayLineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 10, WJScreenW, 0.5)];
    grayLineView1.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [headerView addSubview:grayLineView1];
    
    
    UIView *grayLineView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 74, WJScreenW, 0.5)];
    grayLineView2.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [headerView addSubview:grayLineView2];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 100, 64)];
    label.text = @"转出金额:";
    label.font = [UIFont systemFontOfSize:17];
    [headerView addSubview:label];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(WJScreenW-20-200, 10, 200, 65)];
    textField.placeholder = @"请输入转出金额";
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.textAlignment = UITextAlignmentRight;
    textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    textField.delegate = self;
    [headerView addSubview:textField];

}

-(void)getFooterView
{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 120)];
    self.footerView = footerView;
    
    UIView *grayLineView = [[UIView alloc]initWithFrame:CGRectMake(14, 0, WJScreenW-14, 0.5)];
    grayLineView.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [footerView addSubview:grayLineView];
    
    UIButton *affirmBtn = [[UIButton alloc]initWithFrame:CGRectMake((WJScreenW-111)*0.5, 70, 111, 34)];
    [affirmBtn setImage:[UIImage imageNamed:@"qrtx"] forState:UIControlStateNormal];
    [affirmBtn addTarget:self action:@selector(affirmBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:affirmBtn];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TixianCell *cell = [TixianCell tixianViewCellWithTableView:tableView];
    [self.cellArr addObject:cell];


    cell.nameLabel.text = indexPath.row == 0 ? @"支付宝" : @"微信钱包";
    cell.detailsLabel.text = indexPath.row == 0 ? @"将账户余额提现到支付宝账户中" : @"将账户余额提现到微信账户中";
     cell.iconImage.image = indexPath.row == 0 ? [UIImage imageNamed:@"gr-cz-zfb"] : [UIImage imageNamed:@"gr-cz-wx"];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.cellArr enumerateObjectsUsingBlock:^(TixianCell *cell, NSUInteger idx, BOOL * _Nonnull stop) {
        if (indexPath.row == idx) {
            cell.selecteImage.image = [UIImage imageNamed:@"hongdian"];
            
        } else {
            cell.selecteImage.image = [UIImage imageNamed:@"huiquan"];
        }
    }];
}


-(void)affirmBtnAct
{
    TixianSuccessViewController *tixianSVC = [[TixianSuccessViewController alloc]initWithNibName:@"TixianSuccessViewController" bundle:nil];
    [self.navigationController pushViewController:tixianSVC animated:YES];
}


#pragma mark - 限制textField只能输入数字
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self validateNumber:string];
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
