//
//  PhoneRecordViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "PhoneRecordViewController.h"
#import "PhoneRecordCell.h"

@interface PhoneRecordViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArray;
}

@property (weak, nonatomic) IBOutlet UITableView *phoneRecordTable;

@property (nonatomic, retain) NSMutableArray *dataArray;

@end

@implementation PhoneRecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"通话记录";
    
    [self setExtraCellLineHidden:_phoneRecordTable];
    
        dataArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",nil];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    PhoneRecordCell *cell = [PhoneRecordCell phoneRecordCellViewCellWithTableView:tableView];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}



- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}


-(NSArray<UITableViewRowAction*>*)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewRowAction *rowActionSec = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"黑名单"
                                                                          handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
                                                                              NSLog(@"黑名单");
                                                                          }];
    rowActionSec.backgroundColor = [UIColor lightGrayColor];
    
    
    UITableViewRowAction *rowActionDelete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault  title:@"删除" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
        
        [dataArray removeObjectAtIndex:indexPath.row];
        [self.phoneRecordTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        NSLog(@"删除");
    }];
    
    
    NSArray *arr = @[rowActionSec,rowActionDelete];
    return arr;
}


//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}


@end
