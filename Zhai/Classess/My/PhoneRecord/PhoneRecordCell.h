//
//  PhoneRecordCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhoneRecordCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *phoneImage;
@property (weak, nonatomic) IBOutlet UILabel *phoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *phoneTimelabel;
@property (weak, nonatomic) IBOutlet UILabel *phoneTimeLongLabel;

+(instancetype)phoneRecordCellViewCellWithTableView:(UITableView *)tableView;

@end
