//
//  PhoneRecordCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "PhoneRecordCell.h"

@implementation PhoneRecordCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)phoneRecordCellViewCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"PhoneRecordCell";
    PhoneRecordCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"PhoneRecordCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
