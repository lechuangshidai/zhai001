//
//  SuggestionViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "SuggestionViewController.h"
#import "TextView.h"
#import "AFManager.h"
@interface SuggestionViewController ()<UITextViewDelegate>

@property (strong,nonatomic) TextView *textView;

@property (strong,nonatomic) UIButton *submitBtn;

@end

@implementation SuggestionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"意见反馈";
    
    //添加自定义的textView
    [self setupTextView];
    
    [self getSubmitBtn];
    

}

-(void)getSubmitBtn
{
    UIButton *submitBtn = [[UIButton alloc]initWithFrame:CGRectMake((WJScreenW-112)*0.5, 170, 112, 35)];
    [submitBtn setImage:[UIImage imageNamed:@"tj"] forState:UIControlStateNormal];
    [submitBtn addTarget:self action:@selector(submitBtnAct:) forControlEvents:UIControlEventTouchUpInside];
    self.submitBtn = submitBtn;
    [self.view addSubview:submitBtn];
}


-(void)setupTextView
{
    TextView *textView = [[TextView alloc]initWithFrame:CGRectMake(20, 15, WJScreenW-40, 110)];
    textView.delegate = self;
    textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textView.layer.borderWidth = 1.0f;
    textView.layer.cornerRadius = 5.0f;
    textView.placeholder = @"请描述您遇到的问题状况";
    self.textView = textView;
    [self.view addSubview:textView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChange) name:UITextViewTextDidChangeNotification object:textView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


// 监听textView文字改变，改变发送按钮的状态
-(void)textChange
{
    self.navigationItem.rightBarButtonItem.enabled = self.textView.text.length != 0;
}


-(void)keyboardWillShow:(NSNotification *)notification
{
    // 取出键盘弹出的时间
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:7 << 16 animations:^{
        
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    // 取出键盘隐藏的时间
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:7 << 16 animations:^{
        
        
    } completion:^(BOOL finished) {
        
    }];
}


-(void)submitBtnAct:(UIButton *)btn
{
    [AFManager postReqURL:YONGHUFANKUI reqBody:@{@"id":@"11",@"content":_textView.text} block:^(id infor) {
        NSLog(@"infor====%@",infor);
        if ([[NSString stringWithFormat:@"%@",infor[@"code"]] isEqualToString:@"200"]) {
            
            [self.view endEditing:true];
            
            UIImageView *succeed = [[UIImageView alloc]initWithFrame:CGRectMake((WJScreenW-162)*0.5, 170, 162, 58)];
            succeed.image = [UIImage imageNamed:@"tijiaosuccess"];
            succeed.alpha = 1.0;
            
            UIView *cover = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, WJScreenH)];
            cover.backgroundColor = [UIColor lightGrayColor];
            cover.alpha = 1.0;
            [self.view addSubview:cover];
            
            [cover addSubview:succeed];
            
            [UIView animateWithDuration:2.0 animations:^{
                
                
                cover.alpha = 0.0;
                succeed.alpha = 0.0;
                
                
            } completion:^(BOOL finished) {
                
                [cover removeFromSuperview];
                [self.navigationController popToRootViewControllerAnimated:YES];
                
            }];
        }
    }];
   
}


#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}

@end
