//
//  MyViewController.m
//  Zhai
//
//  Created by 周文静 on 16/7/28.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "MyViewController.h"

#import "SelfRoomViewController.h"


#import "ChongzhiViewController.h"
#import "TixianViewController.h"

#import "MoneyViewController.h"
#import "PhoneRecordViewController.h"
#import "SuggestionViewController.h"
#import "SettingViewController.h"

@interface MyViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *myTable;

@property (strong,nonatomic)UIView *headerView;

@property (strong,nonatomic) UIImageView *iconImage;

@end

@implementation MyViewController

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 删除系统自带的tabBarButton
    for (UIView *tabBarButton in self.tabBarController.tabBar.subviews) {
        
        if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
            
            [tabBarButton removeFromSuperview];
        }
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"我的";
    
    [self setExtraCellLineHidden:_myTable];
    
    [self getHeaderView];
    
    self.myTable.tableHeaderView = self.headerView;
}

-(void)getHeaderView
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW,185)];
    self.headerView = headerView;
    
    UIImageView *imageBg = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 185)];
    imageBg.userInteractionEnabled = YES;
    imageBg.image = [UIImage imageNamed:@"gr-bj"];
    [headerView addSubview:imageBg];
    
    
    UILabel *nameLabel = [[UILabel alloc]initWithFrame:CGRectMake(50, 5, WJScreenW-100, 25)];
    nameLabel.textColor = [UIColor whiteColor];
    nameLabel.text = @"372084y7823074029";
    nameLabel.textAlignment = NSTextAlignmentCenter;
    [imageBg addSubview:nameLabel];
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake((WJScreenW-90)*0.5, 35, 90, 90)];
    iconImage.image = [UIImage imageNamed:@"fx-tx5"];
    iconImage.layer.cornerRadius = 45;
    iconImage.clipsToBounds = YES;
    iconImage.layer.borderColor =[UIColor whiteColor].CGColor;
    iconImage.layer.borderWidth = 1;
    self.iconImage = iconImage;
    [imageBg addSubview:iconImage];
    
    
    UIButton *iconBtn = [[UIButton alloc]initWithFrame:CGRectMake((WJScreenW-90)*0.5, 0, 90, 120)];
    iconBtn.backgroundColor = [UIColor clearColor];
    [iconBtn addTarget:self action:@selector(iconBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [imageBg addSubview:iconBtn];
    
    
   //左边的3个控件
    UILabel *alreadyLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 90, 100, 25)];
    alreadyLabel.text = @"已赚金币";
    alreadyLabel.textAlignment = NSTextAlignmentCenter;
    [imageBg addSubview:alreadyLabel];
    
    UILabel *alreadyNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(20, 120, 100, 25)];
    alreadyNumberLabel.text = @"$564.00";
    alreadyNumberLabel.textColor = [UIColor redColor];
    alreadyNumberLabel.textAlignment = NSTextAlignmentCenter;
    [imageBg addSubview:alreadyNumberLabel];
    
    UIButton *tixianBtn = [[UIButton alloc]initWithFrame:CGRectMake(35, 150, 70, 25)];
    [tixianBtn setImage:[UIImage imageNamed:@"gr-tx"] forState:UIControlStateNormal];
    [tixianBtn addTarget:self action:@selector(tianxianBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [imageBg addSubview:tixianBtn];
    
    
    //右边的3个控件
    UILabel *accountLabel = [[UILabel alloc]initWithFrame:CGRectMake(WJScreenW-120, 90, 100, 25)];
    accountLabel.text = @"账户金币";
    accountLabel.textAlignment = NSTextAlignmentCenter;
    [imageBg addSubview:accountLabel];
    
    UILabel *accountNumberLabel = [[UILabel alloc]initWithFrame:CGRectMake(WJScreenW-120, 120, 100, 25)];
    accountNumberLabel.text = @"$564.00";
    accountNumberLabel.textColor = [UIColor redColor];
    accountNumberLabel.textAlignment = NSTextAlignmentCenter;
    [imageBg addSubview:accountNumberLabel];
    
    UIButton *chongzhiBtn = [[UIButton alloc]initWithFrame:CGRectMake(WJScreenW-105, 150, 70, 25)];
    [chongzhiBtn setImage:[UIImage imageNamed:@"gr-cz"] forState:UIControlStateNormal];
    [chongzhiBtn addTarget:self action:@selector(chongzhiBtnBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [imageBg addSubview:chongzhiBtn];
    
    UIView *grayLine = [[UIView alloc]initWithFrame:CGRectMake(0, 184, WJScreenW, 0.5)];
    grayLine.backgroundColor = [UIColor lightGrayColor];
    [imageBg addSubview:grayLine];
    
}

//点击头像
-(void)iconBtnAct
{
    SelfRoomViewController *selfRoomVC = [[SelfRoomViewController alloc]initWithNibName:@"SelfRoomViewController" bundle:nil];
    [self.navigationController pushViewController:selfRoomVC animated:YES];
}

//提现
-(void)tianxianBtnAct
{
    TixianViewController *tixianVC = [[TixianViewController alloc]initWithNibName:@"TixianViewController" bundle:nil];
    [self.navigationController pushViewController:tixianVC animated:YES];
}

//充值
-(void)chongzhiBtnBtnAct
{
    ChongzhiViewController *chongzhiVC = [[ChongzhiViewController alloc]initWithNibName:@"ChongzhiViewController" bundle:nil];
    [self.navigationController pushViewController:chongzhiVC animated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
   
    static NSString *str = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:str];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    NSArray *nameArr = @[@"接电话赚钱",@"通话记录",@"据说这里有福利!",@"意见反馈",@"设置"];
    cell.textLabel.text = nameArr[indexPath.row];
    
    NSString *imageName = [NSString stringWithFormat:@"gr-%ld",indexPath.row+1];
    cell.imageView.image = [UIImage imageNamed:imageName];
    
    return cell;

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row == 0) {
        
        MoneyViewController *moneyVC = [[MoneyViewController alloc]initWithNibName:@"MoneyViewController" bundle:nil];
        [self.navigationController pushViewController:moneyVC animated:YES];

    }else if (indexPath.row == 1)
    {
        PhoneRecordViewController *phoneRecordVC = [[PhoneRecordViewController alloc]initWithNibName:@"PhoneRecordViewController" bundle:nil];
        [self.navigationController pushViewController:phoneRecordVC animated:YES];
    }else if (indexPath.row == 2)
    {
        
    }else if (indexPath.row == 3)
    {
        SuggestionViewController *suggestionVC = [[SuggestionViewController alloc]initWithNibName:@"SuggestionViewController" bundle:nil];
        [self.navigationController pushViewController:suggestionVC animated:YES];
        
    }else {
        SettingViewController *settingVC = [[SettingViewController alloc]initWithNibName:@"SettingViewController" bundle:nil];
        [self.navigationController pushViewController:settingVC animated:YES];
    }
}

//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}


@end
