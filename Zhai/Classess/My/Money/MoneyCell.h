//
//  MoneyCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoneyCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UIImageView *selectImage;

+(instancetype)moneyCellWithTableView:(UITableView *)tableView;

@end
