//
//  MoneyViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "MoneyViewController.h"
#import "MoneyCell.h"

@interface MoneyViewController ()

@property (weak, nonatomic) IBOutlet UITableView *moneyTable;

@property (strong,nonatomic) NSArray *moneyArr;

@property (strong,nonatomic) NSMutableArray *cellArr;

@property (copy,nonatomic) NSString *strMoney;

@end


@implementation MoneyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置价钱";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnAct)];
    
    self.moneyArr = @[@"0.5元/1分钟",@"1.0元/1分钟",@"1.5元/1分钟",@"2.0元/1分钟",@"2.5元/1分钟",@"3.0元/1分钟",];
    
    self.cellArr = [[NSMutableArray alloc]initWithCapacity:10];
    
    [self setExtraCellLineHidden:_moneyTable];
    
}

-(void)saveBtnAct
{
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  self.moneyArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MoneyCell *cell = [MoneyCell moneyCellWithTableView:tableView];
    
    [self.cellArr addObject:cell];
    
    cell.moneyLabel.text = self.moneyArr[indexPath.row];
    cell.selectImage.image = [UIImage imageNamed:@"huiquan"];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.cellArr enumerateObjectsUsingBlock:^(MoneyCell *cell, NSUInteger idx, BOOL * _Nonnull stop) {
        if (indexPath.row == idx) {
            cell.selectImage.image = [UIImage imageNamed:@"hongdian"];
            self.strMoney = cell.moneyLabel.text;
            
            NSLog(@"self.strMoney == %@",self.strMoney);
            
        } else {
            cell.selectImage.image = [UIImage imageNamed:@"huiquan"];
        }
    }];
}


//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}



@end
