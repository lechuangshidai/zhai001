//
//  ChongzhiViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "ChongzhiViewController.h"
#import "TixianCell.h"
#import "ChongzhiDetaileController.h"

@interface ChongzhiViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (strong,nonatomic) UIView *headerView;
@property (strong,nonatomic) UIView *footerView;

@property (strong,nonatomic) NSMutableArray *cellArr;

@end

@implementation ChongzhiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"充值";
    
    [self getHeaderView];
    
    [self getFooterView];
    
    self.chongzhiTable.tableHeaderView = self.headerView;
    self.chongzhiTable.tableFooterView = self.footerView;
    
    self.cellArr = [[NSMutableArray alloc]initWithCapacity:10];

}

-(void)getHeaderView
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 75)];
    self.headerView = headerView;
    
    UIView *grayLineView1 = [[UIView alloc]initWithFrame:CGRectMake(0, 10, WJScreenW, 0.5)];
    grayLineView1.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [headerView addSubview:grayLineView1];
    
    
    UIView *grayLineView2 = [[UIView alloc]initWithFrame:CGRectMake(0, 74, WJScreenW, 0.5)];
    grayLineView2.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [headerView addSubview:grayLineView2];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(20, 10, 100, 64)];
    label.text = @"充值金额:";
    label.font = [UIFont systemFontOfSize:17];
    [headerView addSubview:label];
    
    UITextField *textField = [[UITextField alloc]initWithFrame:CGRectMake(WJScreenW-20-200, 10, 200, 65)];
    textField.placeholder = @"请输入充值金额";
    textField.borderStyle = UITextBorderStyleNone;
    textField.clearButtonMode = UITextFieldViewModeAlways;
    textField.textAlignment = UITextAlignmentRight;
    textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    textField.delegate = self;
    [headerView addSubview:textField];
    
}

-(void)getFooterView
{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 120)];
    self.footerView = footerView;
    
    UIView *grayLineView = [[UIView alloc]initWithFrame:CGRectMake(14, 0, WJScreenW-14, 0.5)];
    grayLineView.backgroundColor = [UIColor wjColorFloat:@"c1c1c1"];
    [footerView addSubview:grayLineView];
    
    UIButton *affirmBtn = [[UIButton alloc]initWithFrame:CGRectMake((WJScreenW-111)*0.5, 70, 111, 34)];
    [affirmBtn setImage:[UIImage imageNamed:@"nextBtn"] forState:UIControlStateNormal];
    [affirmBtn addTarget:self action:@selector(affirmBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:affirmBtn];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TixianCell *cell = [TixianCell tixianViewCellWithTableView:tableView];
    [self.cellArr addObject:cell];
    
    
    cell.nameLabel.text = indexPath.row == 0 ? @"支付宝" : @"微信钱包";
    cell.detailsLabel.text = indexPath.row == 0 ? @"从支付宝账户中充值到账户余额" : @"从微信账户中充值到账户余额";
    cell.iconImage.image = indexPath.row == 0 ? [UIImage imageNamed:@"gr-cz-zfb"] : [UIImage imageNamed:@"gr-cz-wx"];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.cellArr enumerateObjectsUsingBlock:^(TixianCell *cell, NSUInteger idx, BOOL * _Nonnull stop) {
        if (indexPath.row == idx) {
            cell.selecteImage.image = [UIImage imageNamed:@"hongdian"];
            
        } else {
            cell.selecteImage.image = [UIImage imageNamed:@"huiquan"];
        }
    }];
}


-(void)affirmBtnAct
{
    ChongzhiDetaileController *chongzhiDetaileVC = [[ChongzhiDetaileController alloc]initWithNibName:@"ChongzhiDetaileController" bundle:nil];
    [self.navigationController pushViewController:chongzhiDetaileVC animated:YES];
}


#pragma mark - 限制textField只能输入数字
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return [self validateNumber:string];
}

- (BOOL)validateNumber:(NSString*)number {
    BOOL res = YES;
    NSCharacterSet* tmpSet = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    int i = 0;
    while (i < number.length) {
        NSString * string = [number substringWithRange:NSMakeRange(i, 1)];
        NSRange range = [string rangeOfCharacterFromSet:tmpSet];
        if (range.length == 0) {
            res = NO;
            break;
        }
        i++;
    }
    return res;
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}




@end
