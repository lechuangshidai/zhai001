//
//  ChongzhiDetaileController.m
//  Zhai
//
//  Created by 周文静 on 16/8/15.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "ChongzhiDetaileController.h"
#import "MyViewController.h"

@interface ChongzhiDetaileController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *finishBtn;
- (IBAction)finishBtnAct:(id)sender;

@end

@implementation ChongzhiDetaileController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"充值详情";
    
}


- (IBAction)finishBtnAct:(id)sender {

     [self.navigationController popToRootViewControllerAnimated:YES];
}
@end
