//
//  FriendCircleViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/26.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendCircleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;

- (IBAction)cancelBtnAct:(id)sender;
- (IBAction)sendBtnAct:(id)sender;
@end
