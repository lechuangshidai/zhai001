//
//  FriendCircleViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/26.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "FriendCircleViewController.h"
#import "TextView.h"
#import "PictureViewController.h"
#import "AFManager.h"
#import "AFHTTPSessionManager.h"
@interface FriendCircleViewController ()<UITextViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    PictureViewController *vc;
    NSMutableArray *urlArr;

}
@property (strong,nonatomic) TextView *textView;
@property (strong,nonatomic) UIButton *photoBtn;

@end

@implementation FriendCircleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

    //添加自定义的textView
    [self setupTextView];
    
    
    
    vc = [[PictureViewController alloc] init];
    [self addChildViewController:vc];
    self.view.frame = vc.view.frame;
    [self.view addSubview:vc.pictureCollectonView];
    urlArr = [[NSMutableArray alloc]init];

}


-(void)setupTextView
{
    TextView *textView = [[TextView alloc]initWithFrame:CGRectMake(10, 70, WJScreenW-20, 120)];
    textView.delegate = self;
    textView.placeholder = @"说点什么吧...";
    self.textView = textView;
    [self.view addSubview:textView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChange) name:UITextViewTextDidChangeNotification object:textView];
    
}

// 监听textView文字改变，改变发送按钮的状态
-(void)textChange
{
    self.navigationItem.rightBarButtonItem.enabled = self.textView.text.length != 0;
}

#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}


// 将要拖拽的时候调一次
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}



- (IBAction)cancelBtnAct:(id)sender {
     [_textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)sendBtnAct:(id)sender {

            if (vc.itemsSectionPictureArray.count>0) {
                // 有图片
                [self composeWithImage];
            }
            else
            {
                // 没图片
                [self composeWithoutImage];
            }
            
    
   
}

// 发有图片的微博
-(void)composeWithImage
{
//    for(NSInteger i = 0; i < vc.itemsSectionPictureArray.count; i++)
//    {
//        NSData * imageData =UIImageJPEGRepresentation([vc.itemsSectionPictureArray objectAtIndex: i], 0.01);
//        [AFManager upLoadpath:KURL_FORMAT(LOCAL_HOST_URL,  Upload_Image_Url) reqBody:nil file:imageData fileName:@"file" fileType:@"image/jpg" block:^(id infor) {
//            NSLog(@"====%@",infor);
//            NSString *inforStr = [infor stringByReplacingOccurrencesOfString:@"\\" withString:@""];
//            NSString *inforStr2 = [inforStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//            [urlArr addObject:inforStr2];
//            //        "http:\/\/139.224.43.42\/zhai\/Uploads\/18b9c171ee7bd29789670f66721b5984.png"
//            NSLog(@"url===%@",urlArr);
//        } errorBlock:^(NSError *error) {
//            
//        }];
//    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"text/html",@"application/json",nil];
        NSUserDefaults *mySettingDataR = [NSUserDefaults standardUserDefaults];
        NSString *uid = [mySettingDataR objectForKey:@"uid"];
        NSDictionary *dict = @{@"uid":uid,@"content":@""};
        [manager POST:FABUDONGTAI parameters:dict constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
                    // 上传 多张图片
                    for(NSInteger i = 0; i < vc.itemsSectionPictureArray.count; i++)
                    {
                        NSData * imageData =UIImageJPEGRepresentation([vc.itemsSectionPictureArray objectAtIndex: i], 0.01);
                        // 上传的参数名
                        NSString * Name = [NSString stringWithFormat:@"img%ld", (long)i];
                        // 上传filename
                        NSString * fileName = [NSString stringWithFormat:@"%@.png", Name];
            
                        [formData appendPartWithFileData:imageData name:@"pic" fileName:fileName mimeType:@"image/png"];
                    }
            
        } progress:^(NSProgress * _Nonnull uploadProgress) {
            
        } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
            
            NSLog(@"---%@---",responseObject);
            
        } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
            NSLog(@"%@",error.description);
            
        }];
        

    });
    
    NSLog(@"%@",@"有图");
    [_textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 发没有图片的微博
-(void)composeWithoutImage
{
    NSLog(@"%@",@"无图");

    [AFManager postReqURL:FABUDONGTAI reqBody:@{@"uid":@"11",@"content":@"测试测试"} block:^(id infor) {
        NSLog(@"infor====%@",infor);
        
        
    }];
    
    
    [_textView resignFirstResponder];
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
