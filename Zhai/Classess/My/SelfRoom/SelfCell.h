//
//  SelfCell.h
//  Zhai
//
//  Created by lhb on 16/10/27.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelfCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lable;

@property (strong, nonatomic) IBOutlet UIButton *button;
+(instancetype)selfOneCellWithTableView:(UITableView *)tableView;

-(void)setIntroductionText:(NSString*)text;
@end
