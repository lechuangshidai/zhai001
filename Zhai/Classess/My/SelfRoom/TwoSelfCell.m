//
//  TwoSelfCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/23.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "TwoSelfCell.h"

@interface TwoSelfCell ()

@property(strong,nonatomic) UIImageView *singleImage;
@property(strong,nonatomic) UIImageView *doubleImageOne;
@property(strong,nonatomic) UIImageView *doubleImageTwo;

@end
@implementation TwoSelfCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.introLabel.text = @"4564654465465546544131546541315413154654131546541314131335453454534564565645646545641321321";
    
    
    UIImageView *singleImage = [[UIImageView alloc]init];
    singleImage.frame = CGRectMake(0, 0, 123, 123);
    [self.pictureView addSubview:singleImage];
    singleImage.image = [UIImage imageNamed:@"gr-cz-wx"];
    self.singleImage = singleImage;
    


//    UIImageView *doubleImageOne = [[UIImageView alloc]init];
//    doubleImageOne.frame = CGRectMake(0, 0, 61, 123);
//    [self.pictureView addSubview:doubleImageOne];
//    doubleImageOne.image = [UIImage imageNamed:@"gr-cz-wx"];
//    self.doubleImageOne = doubleImageOne;
//    
//    UIImageView *doubleImageTwo = [[UIImageView alloc]init];
//    doubleImageTwo.frame = CGRectMake(61, 0, 62, 123);
//    [self.pictureView addSubview:doubleImageTwo];
//    doubleImageTwo.image = [UIImage imageNamed:@"gr-cz-wx"];
//    self.doubleImageTwo = doubleImageTwo;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


+(instancetype)selfTwoCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIconTwo = @"TwoSelfCell";
    TwoSelfCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIconTwo];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TwoSelfCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (IBAction)deleBtnAct:(id)sender {
    NSLog(@"点击删除了");
}

@end
