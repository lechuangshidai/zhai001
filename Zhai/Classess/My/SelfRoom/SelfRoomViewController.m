//
//  SelfRoomViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/10.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "SelfRoomViewController.h"
#import "NSObject+SelectedVc.h"
#import "SelfCell.h"
#import "TwoSelfCell.h"
#import "IconViewController.h"
#import "FriendCircleViewController.h"
#import "AFManager.h"



@interface SelfRoomViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArr;
}
@property (weak, nonatomic) IBOutlet UITableView *selfRoomTable;
@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) NSArray *allDatas;

@property(nonatomic, strong) NSArray *arrImg;


@end

@implementation SelfRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"个人资料";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"fix"] style:UIBarButtonItemStylePlain target:self action:@selector(fixBtnAct)];
    NSString *uid = [[NSUserDefaults standardUserDefaults]objectForKey:@"uid"];
    dataArr = [[NSMutableArray alloc]init];

    [AFManager getReqURL:[NSString stringWithFormat:DONGTAILIEBIAO,uid] block:^(id infor) {
        NSLog(@"--%@",infor);
        [dataArr addObjectsFromArray:infor[@"data"]];
        [self.selfRoomTable reloadData];
    } errorblock:^(NSError *error) {
        NSLog(@"--%@--",error);
    }];

    _arrImg = @[@"gr-cz-wx",
                        @"gr-cz-wx",
                        @"gr-cz-wx",
                        @"gr-cz-zfb",
                        @"gr-cz-wx",
                        @"gr-cz-wx"];
    
    [self gainHeaderView];
    
    self.selfRoomTable.tableHeaderView =self.headerView;
    
  
    [self setExtraCellLineHidden:self.selfRoomTable];
    
}


-(void)fixBtnAct
{
    IconViewController *iconVC = [[IconViewController alloc]initWithNibName:@"IconViewController" bundle:nil];
    [self.navigationController pushViewController:iconVC animated:YES];
}


-(void)gainHeaderView
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 206)];
    self.headerView = headerView;
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 80, 80)];
    iconImage.image = [UIImage imageNamed:@"fx-tx5"];
    iconImage.layer.cornerRadius = 40;
    iconImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    iconImage.layer.borderWidth = 2.0f;
    [headerView addSubview:iconImage];
    
    
    UILabel *nameLabel = [[UILabel alloc]init];
    nameLabel.font = [UIFont systemFontOfSize:17];
    nameLabel.text = @"名54413字";
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.numberOfLines = 1;
    nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    CGSize maxLabelSize = CGSizeMake(180, 25);
    CGSize expectSize = [nameLabel sizeThatFits:maxLabelSize];
    nameLabel.frame = CGRectMake(110, 30, expectSize.width, expectSize.height);
    [headerView addSubview:nameLabel];
    
    UIImageView *sexImage = [[UIImageView alloc]initWithFrame:CGRectMake(130+expectSize.width, 32, 13, 15)];
    sexImage.image = [UIImage imageNamed:@"nv"];
    [headerView addSubview:sexImage];
    
    UILabel *ageLabel = [[UILabel alloc]initWithFrame:CGRectMake(160+expectSize.width, 30, 21, 21)];
    ageLabel.font = [UIFont systemFontOfSize:12];
    ageLabel.text = @"23";
    ageLabel.textColor = [UIColor grayColor];
    [headerView addSubview:ageLabel];
    
    UILabel *declarationLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 60, WJScreenW-180, 25)];
    declarationLabel.font = [UIFont systemFontOfSize:13];
    NSString *strDeclaration = @"378247893024";
    declarationLabel.text = [NSString stringWithFormat:@"聊天宣言:%@",strDeclaration];
    declarationLabel.textColor = [UIColor grayColor];
    [headerView addSubview:declarationLabel];
    
    UIView *grayBg = [[UIView alloc]initWithFrame:CGRectMake(20, 105, WJScreenW-40, 100)];
    grayBg.backgroundColor = [UIColor wjColorFloat:@"f2f2f2"];
    [headerView addSubview:grayBg];
    
    UILabel *todayLable = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 50, 25)];
    todayLable.text = @"今天";
    [grayBg addSubview:todayLable];
    
    UIButton *photoBtn = [[UIButton alloc]initWithFrame:CGRectMake(55, 20, 89, 68)];
    [photoBtn setImage:[UIImage imageNamed:@"photo"] forState:UIControlStateNormal];
    [photoBtn addTarget:self action:@selector(photoBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [grayBg addSubview:photoBtn];
    
    
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0||indexPath.row == 1) {
     return 60;
    }else
    {
        return 160;
    }
   
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0||indexPath.row == 1) {
        SelfCell *cell = [SelfCell selfOneCellWithTableView:tableView];
        cell.lable.text = dataArr[indexPath.row][@"content"];
        [cell setIntroductionText:cell.lable.text];
        return cell;
    }else
    {
          TwoSelfCell *cell = [TwoSelfCell selfTwoCellWithTableView:tableView];
        
        return cell;
    }
 
  

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
        [SelfRoomViewController js_imgViewScaleWithImgUrlArr:_arrImg andPlaceImgStr:@"back"];
}

-(void)photoBtnAct
{

    FriendCircleViewController *friendCircleVC = [[FriendCircleViewController alloc]initWithNibName:@"FriendCircleViewController" bundle:nil];
    
    friendCircleVC.modalPresentationStyle = UIModalPresentationPopover;
    
    [self presentViewController:friendCircleVC animated:YES completion:nil];
}

//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
}



@end
