//
//  PersonalData.m
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "PersonalData.h"

@implementation PersonalData
-(id)initWithDictionary:(NSDictionary *)aDic
{
    self = [super init];
    if (self)
    {
        self.age = aDic[@"age"];
        self.area = aDic[@"area"];
        self.autoStr = aDic[@"auto"];
        self.head_pic = aDic[@"head_pic"];
        self.nickname = aDic[@"nickname"];
        self.sex = aDic[@"sex"];
    }
    return self;
}
+(id)personalDataDictionary:(NSDictionary *)aDic
{
    return [[self alloc] initWithDictionary:aDic];
}
@end
