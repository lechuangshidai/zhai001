//
//  PersonalShowInfo.m
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "PersonalShowInfo.h"

@implementation PersonalShowInfo
-(id)initWithDictionary:(NSDictionary *)aDic
{
    self =[super init];
    if (self)
    {
        self.code = aDic[@"code"];
        self.data = aDic[@"data"];
    }
    return self;
}
+(id)personalCodeDictionary:(NSDictionary *)aDic
{
    return [[self alloc] initWithDictionary:aDic];
}
@end
