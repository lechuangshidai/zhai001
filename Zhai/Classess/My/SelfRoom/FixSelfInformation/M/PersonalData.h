//
//  PersonalData.h
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PersonalData : NSObject
@property(nonatomic,strong)NSString *age, *area, *autoStr, *head_pic, *nickname, *sex;

+(id)personalDataDictionary:(NSDictionary *)aDic;
@end
