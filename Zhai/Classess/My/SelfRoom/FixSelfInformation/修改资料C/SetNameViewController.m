//
//  SetNameViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "SetNameViewController.h"


@interface SetNameViewController ()

@end

@implementation SetNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"名字";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnAct)];
    self.nameText.text = self.nameStr;
}

-(void)saveBtnAct
{
    if (self.nameTextBlock != nil) {
        self.nameTextBlock(self.nameText.text);
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}


-(void)nameText:(NameTextBlock)block
{
    self.nameTextBlock = block;
}

@end
