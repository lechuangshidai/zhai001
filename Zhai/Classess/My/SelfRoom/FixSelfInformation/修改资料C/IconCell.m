//
//  IconCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/5.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "IconCell.h"

@implementation IconCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    
    self.iconImage.layer.cornerRadius = 30.0;
    self.iconImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.iconImage.layer.borderWidth = 2.0f;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)iconViewCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"IconCell";
    IconCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"IconCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}



@end
