//
//  DeclarationViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/9.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "DeclarationViewController.h"


@interface DeclarationViewController ()<UITextViewDelegate>





@end

@implementation DeclarationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"聊天宣言";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveBtnAct)];
    
    //添加自定义的textView
    [self setupTextView];
}


-(void)setupTextView
{
    TextView *textView = [[TextView alloc]initWithFrame:CGRectMake(20, 15, WJScreenW-40, 50)];
    textView.delegate = self;
    textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    textView.layer.borderWidth = 1.0f;
    textView.layer.cornerRadius = 5.0f;
    textView.placeholder = @"聊天宣言:";
    self.textView = textView;
    [self.view addSubview:textView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(textChange) name:UITextViewTextDidChangeNotification object:textView];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}


// 监听textView文字改变，改变发送按钮的状态
-(void)textChange
{
    self.navigationItem.rightBarButtonItem.enabled = self.textView.text.length != 0;
}


-(void)keyboardWillShow:(NSNotification *)notification
{
    // 取出键盘弹出的时间
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:7 << 16 animations:^{
        
        
    } completion:^(BOOL finished) {
        
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    // 取出键盘隐藏的时间
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration delay:0.0 options:7 << 16 animations:^{
        
        
    } completion:^(BOOL finished) {
        
    }];
}


// 将要拖拽的时候调一次
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    [self.view endEditing:YES];
}



-(void)saveBtnAct
{

    if (self.declarationBlock != nil) {
        self.declarationBlock(self.textView.text);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
}


-(void)declarationText:(DeclarationBlock)block
{
    self.declarationBlock = block;
}

@end
