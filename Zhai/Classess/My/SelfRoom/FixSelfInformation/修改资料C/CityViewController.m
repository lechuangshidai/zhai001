//
//  CityViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/25.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "CityViewController.h"
#import "JWCityGroup.h"
#import "MJExtension.h"
#import "IconViewController.h"
@interface CityViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *cityTable;
@property (strong, nonatomic) NSArray *cityGroups;
@property (copy,nonatomic) NSString *strCity;

@end

@implementation CityViewController

-(NSArray *)cityGroups
{
    if (!_cityGroups) {
        _cityGroups = [JWCityGroup objectArrayWithFilename:@"cityGroups.plist"];
    }
    return _cityGroups;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 设置组索引字体的颜色
    self.cityTable.sectionIndexColor = [UIColor blackColor];
    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return self.cityGroups.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    JWCityGroup *g = self.cityGroups[section];
    return g.cities.count;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *str = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:str];
    }
    JWCityGroup *g = self.cityGroups[indexPath.section];
    
    cell.textLabel.text = g.cities[indexPath.row];
    return cell;
    
    
}

#pragma mark   ==============  header  ==============
-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    JWCityGroup *g = self.cityGroups[section];
    return g.title;
}

-(NSArray *)sectionIndexTitlesForTableView:(UITableView *)tableView
{
    // 把每一组的title装到一个数组里在返回
    return [self.cityGroups valueForKeyPath:@"title"];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    JWCityGroup *cityGroup = self.cityGroups[indexPath.section];
    
    _strCity = [NSString stringWithFormat:@"%@",cityGroup.cities[indexPath.row]];
    
    if (self.cityBlock != nil) {
        self.cityBlock(_strCity);
    }
//    NSLog(@"%@",self.navigationController.viewControllers);

//    IconViewController *iconVC = self.navigationController.viewControllers[2];
//    iconVC.areaStr = self.strCity;
    [self.navigationController popViewControllerAnimated:YES];
 
}

-(void)cityBlock:(CityBlock)block
{
    self.cityBlock = block;
}

@end
