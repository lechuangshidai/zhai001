//
//  OtherCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/5.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "OtherCell.h"

@implementation OtherCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)otherViewCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"OtherCell";
    OtherCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"OtherCell" owner:self options:nil] lastObject];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
