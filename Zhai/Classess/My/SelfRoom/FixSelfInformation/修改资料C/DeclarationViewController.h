//
//  DeclarationViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/9.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TextView.h"

typedef void (^DeclarationBlock)(NSString *declarationText);
@interface DeclarationViewController : UIViewController

@property (strong,nonatomic) TextView *textView;

@property (strong,nonatomic) DeclarationBlock declarationBlock;
-(void)declarationText:(DeclarationBlock)block;

@end




