//
//  SetNameViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^NameTextBlock)(NSString *nameTextData);

@interface SetNameViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *nameText;

@property (copy, nonatomic) NameTextBlock nameTextBlock;
@property(copy,nonatomic)NSString *nameStr;
-(void)nameText:(NameTextBlock)block;

@end
