//
//  IconCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/5.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImage;

+(instancetype)iconViewCellWithTableView:(UITableView *)tableView;

@end
