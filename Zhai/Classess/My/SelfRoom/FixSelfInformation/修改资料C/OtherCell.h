//
//  OtherCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/5.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OtherCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *selecteLabel;
@property (weak, nonatomic) IBOutlet UIImageView *jianjianImage;

+(instancetype)otherViewCellWithTableView:(UITableView *)tableView;

@end
