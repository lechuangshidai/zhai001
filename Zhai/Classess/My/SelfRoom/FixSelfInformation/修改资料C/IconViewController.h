//
//  IconViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/5.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IconViewController : UIViewController


@property(strong,nonatomic)NSString *imageUrl, *nameStr, *sexStr, *areaStr, *autoStr;
@end
