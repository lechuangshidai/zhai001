//
//  TwoSelfCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/23.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TwoSelfCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UIView *pictureView;
@property (weak, nonatomic) IBOutlet UILabel *introLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleBtn;

@property (strong,nonatomic) NSArray *pictureArr;

- (IBAction)deleBtnAct:(id)sender;

+(instancetype)selfTwoCellWithTableView:(UITableView *)tableView;

@end
