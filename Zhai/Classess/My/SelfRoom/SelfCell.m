//
//  SelfCell.m
//  Zhai
//
//  Created by lhb on 16/10/27.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "SelfCell.h"
#define kIntroLabelFont [UIFont systemFontOfSize:14]

@implementation SelfCell
-(void)setIntroductionText:(NSString*)text{
    //获得当前cell高度
    CGRect frame = [self frame];
    //文本赋值
    self.lable.text = text;
    //设置label的最大行数
    self.lable.numberOfLines = 0;
    
    CGSize size = CGSizeMake(self.lable.size.width, 1000);
    NSDictionary *attributeDict = @{NSFontAttributeName : kIntroLabelFont};
    
    CGSize introLabelSize = [self.lable.text boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin attributes:attributeDict context:nil].size;
    
    self.lable.frame = CGRectMake(self.lable.frame.origin.x, self.lable.frame.origin.y, introLabelSize.width, introLabelSize.height);
    
    //计算出自适应的高度
    frame.size.height = introLabelSize.height+32;
    NSLog(@"--%f",frame.size.height);
    self.frame = frame;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lable.font = kIntroLabelFont;
    self.lable.numberOfLines = 0;
    self.lable.backgroundColor = [UIColor wjColorFloat:@"e2e2e2"];
}
+(instancetype)selfOneCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIconOne = @"SelfCell";
    SelfCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIconOne];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"SelfCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
