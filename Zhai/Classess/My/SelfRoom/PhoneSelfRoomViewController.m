//
//  PhoneSelfRoomViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/25.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "PhoneSelfRoomViewController.h"
#import "NSObject+SelectedVc.h"
#import "SelfCell.h"
#import "TwoSelfCell.h"
#import "IconViewController.h"
#import "AFManager.h"

@interface PhoneSelfRoomViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *dataArr;
}
@property (weak, nonatomic) IBOutlet UITableView *phoneSelfRoomTable;

@property (strong,nonatomic) UIView *headerView;

@property (strong,nonatomic) NSArray *allDatas;

@property(nonatomic, strong) NSArray *arrImg;

@end

@implementation PhoneSelfRoomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
 
    self.title = @"个人资料";
    
    _arrImg = @[        @"gr-cz-wx",
                        @"gr-cz-wx",
                        @"gr-cz-wx",
                        @"gr-cz-zfb",
                        @"gr-cz-wx",
                        @"gr-cz-wx"];
    [AFManager getReqURL:[NSString stringWithFormat:DONGTAILIEBIAO,self.uid] block:^(id infor) {
        NSLog(@"--%@",infor);
        [dataArr addObjectsFromArray:infor[@"data"]];
        [self.phoneSelfRoomTable reloadData];
    } errorblock:^(NSError *error) {
        NSLog(@"--%@--",error);
    }];

    
    [self gainHeaderView];
    
    self.phoneSelfRoomTable.tableHeaderView =self.headerView;
    
    
    [self setExtraCellLineHidden:self.phoneSelfRoomTable];
    
}

-(void)gainHeaderView
{
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 110)];
    self.headerView = headerView;
    
    UIImageView *iconImage = [[UIImageView alloc]initWithFrame:CGRectMake(20, 15, 80, 80)];
    iconImage.image = [UIImage imageNamed:@"fx-tx5"];
    iconImage.layer.cornerRadius = 40;
    iconImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    iconImage.layer.borderWidth = 2.0f;
    [headerView addSubview:iconImage];
    
    
    UILabel *nameLabel = [[UILabel alloc]init];
    nameLabel.font = [UIFont systemFontOfSize:17];
    nameLabel.text = @"名54413字";
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.numberOfLines = 1;
    nameLabel.lineBreakMode = NSLineBreakByTruncatingTail;
    CGSize maxLabelSize = CGSizeMake(180, 25);
    CGSize expectSize = [nameLabel sizeThatFits:maxLabelSize];
    nameLabel.frame = CGRectMake(110, 30, expectSize.width, expectSize.height);
    [headerView addSubview:nameLabel];
    
    UIImageView *sexImage = [[UIImageView alloc]initWithFrame:CGRectMake(130+expectSize.width, 32, 13, 15)];
    sexImage.image = [UIImage imageNamed:@"nv"];
    [headerView addSubview:sexImage];
    
    UILabel *ageLabel = [[UILabel alloc]initWithFrame:CGRectMake(160+expectSize.width, 30, 21, 21)];
    ageLabel.font = [UIFont systemFontOfSize:12];
    ageLabel.text = @"23";
    ageLabel.textColor = [UIColor grayColor];
    [headerView addSubview:ageLabel];
    
    UILabel *declarationLabel = [[UILabel alloc]initWithFrame:CGRectMake(110, 60, WJScreenW-180, 25)];
    declarationLabel.font = [UIFont systemFontOfSize:13];
    NSString *strDeclaration = @"378247893024";
    declarationLabel.text = [NSString stringWithFormat:@"聊天宣言:%@",strDeclaration];
    declarationLabel.textColor = [UIColor grayColor];
    [headerView addSubview:declarationLabel];
    
    UIButton *phoneBtn = [[UIButton alloc]initWithFrame:CGRectMake(WJScreenW - 47, 40, 27, 27)];
    [phoneBtn setImage:[UIImage imageNamed:@"phone"] forState:UIControlStateNormal];
    [phoneBtn addTarget:self action:@selector(phoneBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:phoneBtn];
    
}


-(void)phoneBtn:(UIButton *)btn
{
    NSLog(@"点击了打电话");
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0||indexPath.row == 1) {
        UITableViewCell *cell = [self tableView:_phoneSelfRoomTable cellForRowAtIndexPath:indexPath];
        return cell.frame.size.height;
    }else
    {
        return 160;
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0||indexPath.row == 1) {
        SelfCell *cell = [SelfCell selfOneCellWithTableView:_phoneSelfRoomTable];
        cell.lable.text = dataArr[indexPath.row][@"content"];
        [cell setIntroductionText:cell.lable.text];
        
        return cell;
    }else
    {
        TwoSelfCell *cell = [TwoSelfCell selfTwoCellWithTableView:tableView];
        cell.introLabel.text = @"dddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddddd";
        //[cell setIntroductionText:cell.introLabel.text];
        return cell;
    }

}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [PhoneSelfRoomViewController js_imgViewScaleWithImgUrlArr:_arrImg andPlaceImgStr:@"back"];
}

//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
}



@end
