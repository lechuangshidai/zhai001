//
//  BlacklistViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "BlacklistViewController.h"
#import "BlacklistCell.h"
#import "AFManager.h"
#import "BlackListInfo.h"
@interface BlacklistViewController ()
{
    NSMutableArray *dataArray;
}
@property (nonatomic, retain) NSMutableArray *blackDataArray;

@property (weak, nonatomic) IBOutlet UITableView *blacklistTable;
@end

@implementation BlacklistViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"黑名单";
    
//    dataArray = [[NSMutableArray alloc] initWithObjects:@"1",@"2",@"3",@"4",@"5",nil];
    self.blackDataArray = [NSMutableArray array];
    
    [self setExtraCellLineHidden:_blacklistTable];
    [self getRequestBlackList];
}

-(void)getRequestBlackList
{
    [AFManager getReqURL:[NSString stringWithFormat:HEIMINGDANLIEBIAO,[[NSUserDefaults standardUserDefaults] objectForKey:@"uid" ]] block:^(id infor) {
        NSLog(@"infor===%@",infor);
//        NSMutableArray *dataArray = infor[@"data"];
        BlackListInfo *blackList = [BlackListInfo blackListInfoDictionary:infor];
//        BlackListData *data = [BlackListData BlackListDataDictionary:blackList.data];
//        self.blackDataArray addObjectsFromArray:b
        
        [_blacklistTable reloadData];
    } errorblock:^(NSError *error) {
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return dataArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    BlacklistCell *cell = [BlacklistCell blacklistCellCellWithTableView:tableView];
    
    return cell;
    
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        
        [dataArray removeObjectAtIndex:indexPath.row];
        // Delete the row from the data source.
        [self.blacklistTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
    }
}

//改变按钮上的字
- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath{
    return @"删除";
}


//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}



@end
