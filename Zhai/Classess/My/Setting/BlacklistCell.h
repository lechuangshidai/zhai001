//
//  BlacklistCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BlacklistCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *blacklistImage;
@property (weak, nonatomic) IBOutlet UILabel *blacklistNumberLabel;

@property (weak, nonatomic) IBOutlet UILabel *blacklistTimeLabel;

+(instancetype)blacklistCellCellWithTableView:(UITableView *)tableView;
@end
