//
//  NoteViewController.h
//  Zhai
//
//  Created by 周文静 on 16/8/10.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKCountDownButton.h"

@interface NoteViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITextField *phoneNumber;

@property (weak, nonatomic) IBOutlet JKCountDownButton *yanzhengmaBtn;
- (IBAction)yanzhengmaBtnAct:(JKCountDownButton *)sender;

@end
