//
//  SettingViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "SettingViewController.h"
#import "BlacklistViewController.h"
#import "AlterPasswordViewController.h"
#import "EnterViewController.h"

@interface SettingViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *settingTable;

@property (strong,nonatomic) UIView *footerView;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"设置";
    
    [self setExtraCellLineHidden:_settingTable];
    
    [self getFooterView];
    
    self.settingTable.tableFooterView = self.footerView;
}


-(void)getFooterView
{
    UIView *footerView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WJScreenW, 200)];
    self.footerView = footerView;
    
    UIButton *cancelBtn = [[UIButton alloc]initWithFrame:CGRectMake((WJScreenW-111)*0.5, 150, 111, 34)];
    [cancelBtn setImage:[UIImage imageNamed:@"gr-sz-zx"] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelBtnAct) forControlEvents:UIControlEventTouchUpInside];
    [footerView addSubview:cancelBtn];
    
}

-(void)cancelBtnAct
{
    NSLog(@"注销了");
    [[NSUserDefaults standardUserDefaults] setValue:nil forKey:@"uid"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    EnterViewController *enter = [[EnterViewController alloc]init];
    [self.navigationController pushViewController:enter animated:YES];

    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 61;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *str = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:str];
    }
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    NSArray *nameArr = @[@"修改密码",@"黑名单",@"帮助"];
    cell.textLabel.text = nameArr[indexPath.row];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.row) {
        case 0:
            [self getAlterPasswordVC];
            break;
        case 1:
            [self getBlacklistVC];
            break;
            
        default:
            break;
    }

}
-(void)getAlterPasswordVC
{
    AlterPasswordViewController *alterPasswordVC = [[AlterPasswordViewController alloc]initWithNibName:@"AlterPasswordViewController" bundle:nil];
     [self.navigationController pushViewController:alterPasswordVC animated:YES];
}

-(void)getBlacklistVC
{
    BlacklistViewController *blacklistVC = [[BlacklistViewController alloc]initWithNibName:@"BlacklistViewController" bundle:nil];
    [self.navigationController pushViewController:blacklistVC animated:YES];

}

//隐藏table多余的线
- (void)setExtraCellLineHidden: (UITableView *)tableView{
    
    UIView *view =[ [UIView alloc]init];
    
    view.backgroundColor = [UIColor whiteColor];
    
    [tableView setTableFooterView:view];
    
    [tableView setTableHeaderView:view];
    
}


@end
