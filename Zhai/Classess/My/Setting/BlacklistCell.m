//
//  BlacklistCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "BlacklistCell.h"

@implementation BlacklistCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)blacklistCellCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"BlacklistCell";
    BlacklistCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"BlacklistCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

@end
