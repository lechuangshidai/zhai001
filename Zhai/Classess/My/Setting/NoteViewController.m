//
//  NoteViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/10.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "NoteViewController.h"


@interface NoteViewController ()

@end

@implementation NoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"短信验证";
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]initWithTitle:@"提交" style:UIBarButtonItemStylePlain target:self action:@selector(submitBtnAct)];
    
    [self.phoneNumber addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    
}

- (void)textFieldDidChange:(UITextField *)textField
{
    if (textField == self.phoneNumber) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}


- (IBAction)yanzhengmaBtnAct:(JKCountDownButton *)sender {
    
 
    NSString *strphoneNumber = self.phoneNumber.text;

    if ([strphoneNumber isValidPhoneNumber]) {
        
        NSLog(@"手机号格式正确");
        
        
        sender.enabled = NO;
        //button type要 设置成custom 否则会闪动
        [sender startWithSecond:60];
        
        [sender didChange:^NSString *(JKCountDownButton *countDownButton,int second) {
            NSString *title = [NSString stringWithFormat:@"发送验证码(%d)",second];
            return title;
        }];
        [sender didFinished:^NSString *(JKCountDownButton *countDownButton, int second) {
            countDownButton.enabled = YES;
            return @"点击重新获取";
            
        }];
        
        
        
    }else{
        [NSObject wj_alterSingleVCWithOneTitle:@"提示" andTwoTitle:@"请输入正确的手机号" andSelfVC:self];
    }
}

-(void)submitBtnAct
{
    

}

//点击屏幕收键盘
#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}


@end
