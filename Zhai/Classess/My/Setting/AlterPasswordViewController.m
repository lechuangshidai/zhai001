//
//  AlterPasswordViewController.m
//  Zhai
//
//  Created by 周文静 on 16/8/8.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "AlterPasswordViewController.h"
#import "NoteViewController.h"
#import "AFManager.h"
@interface AlterPasswordViewController ()<UITextFieldDelegate>

@end

@implementation AlterPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = @"修改密码";
    
    self.oldPasswordTextFiled.delegate = self;
    self.textField.delegate = self;
    
    
}


- (IBAction)ShortMessageBtnAct:(id)sender {
    
    NoteViewController *noteVC = [[NoteViewController alloc]initWithNibName:@"NoteViewController" bundle:nil];
    [self.navigationController pushViewController:noteVC animated:YES];
}
- (IBAction)confirmBtnAct:(id)sender {
    
    [AFManager postReqURL:XIUGAIMIMA reqBody:@{@"id":[[NSUserDefaults standardUserDefaults]objectForKey:@"uid"],@"oldPass":self.oldPasswordTextFiled.text,@"newPass":self.textField.text} block:^(id infor) {
        NSLog(@"infor====%@",infor);
        if ([[NSString stringWithFormat:@"%@",infor[@"code"]] isEqualToString:@"200"]) {
           
            
        }
    }];
//    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 点击reture收回键盘
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

//点击屏幕收键盘
#pragma mark - 点击空白处收回键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:true];
}


@end
