//
//  BlackListData.h
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlackListData : NSObject
@property(nonatomic,strong)NSString *bid, *ID, *phone, *time;
+(id)BlackListDataDictionary:(NSDictionary *)aDic;
@end
