//
//  BlackListInfo.h
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BlackListInfo : NSObject
@property(nonatomic,strong)NSString *code;
@property(nonatomic,strong)NSArray *data;
+(id)blackListInfoDictionary:(NSDictionary *)aDic;
@end
