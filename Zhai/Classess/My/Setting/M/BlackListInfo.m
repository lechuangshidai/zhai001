//
//  BlackListInfo.m
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "BlackListInfo.h"

@implementation BlackListInfo
-(id)initWithDictionary:(NSDictionary *)aDic
{
    self =[super init];
    if (self)
    {
        self.code = aDic[@"code"];
        self.data = aDic[@"data"];
    }
    return self;
}
+(id)blackListInfoDictionary:(NSDictionary *)aDic
{
    return [[self alloc] initWithDictionary:aDic];
}
@end
