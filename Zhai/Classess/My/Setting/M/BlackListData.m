//
//  BlackListData.m
//  Zhai
//
//  Created by 张婷 on 16/10/30.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "BlackListData.h"

@implementation BlackListData
-(id)initWithDictionary:(NSDictionary *)aDic
{
    self =[super init];
    if (self)
    {
        self.bid = aDic[@"bid"];
        self.ID = aDic[@"id"];
        self.phone = aDic[@"phone"];
        self.time = aDic[@"time"];
    }
    return self;
}
+(id)BlackListDataDictionary:(NSDictionary *)aDic
{
    return [[self alloc] initWithDictionary:aDic];
}
@end
