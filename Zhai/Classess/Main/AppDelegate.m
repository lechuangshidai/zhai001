//
//  AppDelegate.m
//  Zhai
//
//  Created by 周文静 on 16/7/28.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "AppDelegate.h"
#import "TabBarController.h"
#import "NavViewController.h"
#import "HomeViewController.h"
#import "DiscoverViewController.h"
#import "NearbyViewController.h"
#import "MyViewController.h"

#import "EnterViewController.h"
#import <SMS_SDK/SMSSDK.h>

@interface AppDelegate ()

@end

@implementation AppDelegate

-(void)login{
    TabBarController *tabBarVC = [[TabBarController alloc]init];
    
    
    //初始化中心视图
    HomeViewController * homeVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
    
    DiscoverViewController *discoverVC = [[DiscoverViewController alloc]initWithNibName:@"DiscoverViewController" bundle:nil];
    NearbyViewController *nearbyVC = [[NearbyViewController alloc]initWithNibName:@"NearbyViewController" bundle:nil];
    MyViewController *myVC = [[MyViewController alloc]initWithNibName:@"MyViewController" bundle:nil];
    
    
    NavViewController * navOne = [[NavViewController alloc] initWithRootViewController:homeVC];
    NavViewController * navTwo = [[NavViewController alloc] initWithRootViewController:discoverVC];
    NavViewController * navThree = [[NavViewController alloc] initWithRootViewController:nearbyVC];
    NavViewController * navFour = [[NavViewController alloc] initWithRootViewController:myVC];
    NSArray *array = @[navOne,navTwo,navThree,navFour];
    
    tabBarVC.viewControllers = array;
    self.window.rootViewController = tabBarVC;

}
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    // 0 还原状态栏 yes隐藏  no不隐藏
    application.statusBarHidden = NO;
    
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(login) name:@"login" object:nil];
    
   NSString *uid = [[NSUserDefaults standardUserDefaults]objectForKey:@"uid"];
    if (uid) {
        
        
        TabBarController *tabBarVC = [[TabBarController alloc]init];
        
        
        //初始化中心视图
        HomeViewController * homeVC = [[HomeViewController alloc] initWithNibName:@"HomeViewController" bundle:nil];
        
        DiscoverViewController *discoverVC = [[DiscoverViewController alloc]initWithNibName:@"DiscoverViewController" bundle:nil];
        NearbyViewController *nearbyVC = [[NearbyViewController alloc]initWithNibName:@"NearbyViewController" bundle:nil];
        MyViewController *myVC = [[MyViewController alloc]initWithNibName:@"MyViewController" bundle:nil];
        
        
        NavViewController * navOne = [[NavViewController alloc] initWithRootViewController:homeVC];
        NavViewController * navTwo = [[NavViewController alloc] initWithRootViewController:discoverVC];
        NavViewController * navThree = [[NavViewController alloc] initWithRootViewController:nearbyVC];
        NavViewController * navFour = [[NavViewController alloc] initWithRootViewController:myVC];
        NSArray *array = @[navOne,navTwo,navThree,navFour];
        
        tabBarVC.viewControllers = array;
        self.window.rootViewController = tabBarVC;

    }else{
            EnterViewController *enterVC = [[EnterViewController alloc]initWithNibName:@"EnterViewController" bundle:nil];
            NavViewController * enterVCNav = [[NavViewController alloc] initWithRootViewController:enterVC];
            self.window.rootViewController = enterVCNav;
        
    }
   


    //短信验证
    //初始化应用，appKey和appSecret从后台申请得
    [SMSSDK registerApp:@"167bf45905421"
             withSecret:@"40f4f4b5c7410d48dd7bd77d2025a5ac"];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
