//
//  NearbyCell.h
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BlockButton) ();
@interface NearbyCell : UITableViewCell

+(instancetype)nearbyViewCellWithTableView:(UITableView *)tableView;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;


@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *sexImage;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *rangeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (weak, nonatomic) IBOutlet UILabel *declarationLabel;
@property (weak, nonatomic) IBOutlet UIButton *phoneBtn;
- (IBAction)phoneBtnAct:(id)sender;

@property (strong,nonatomic) BlockButton button;
-(void)nearbyPhoneBtnAct:(BlockButton)block;

@end
