//
//  NearbyCell.m
//  Zhai
//
//  Created by 周文静 on 16/8/6.
//  Copyright © 2016年 lechuangshidai. All rights reserved.
//

#import "NearbyCell.h"

@implementation NearbyCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.iconImage.layer.cornerRadius = 35;
//    self.iconImage.layer.borderWidth = 2;
//    self.iconImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

+(instancetype)nearbyViewCellWithTableView:(UITableView *)tableView
{
    static NSString *CellIdentifierIcon = @"NearbyCell";
    NearbyCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifierIcon];
    if (cell == nil) {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NearbyCell" owner:self options:nil] lastObject];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;

}


- (IBAction)phoneBtnAct:(id)sender {
    
    if (self.button) {
        self.button();
    }
}

-(void)nearbyPhoneBtnAct:(BlockButton)block
{
    self.button = block;
}

@end
